/**
 * utility for ggez
 */
pub mod graphics;
pub mod input;
pub extern crate ggez;
