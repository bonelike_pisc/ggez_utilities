pub struct VertexShaderBuilder {
    header: Vec<u8>,
    body: Vec<u8>,
    whole_source: Vec<u8>,
}

impl VertexShaderBuilder {
    pub fn new_150() -> Self {
        Self {
            header: b"
                #version 150 core

                in vec2 a_Pos;
                in vec2 a_Uv;
            
                in vec4 a_Src;
                in vec4 a_TCol1;
                in vec4 a_TCol2;
                in vec4 a_TCol3;
                in vec4 a_TCol4;
                in vec4 a_Color;

                out vec2 v_Uv;
                out vec4 v_Color;
            
                layout (std140) uniform Globals {
                    mat4 u_MVP;
                };
            ".to_vec(),
            body: b"".to_vec(),
            whole_source: b"".to_vec(),
        }
    }

    pub fn default_body(mut self) -> Self {
        self.body = b"
            void main() {
                v_Uv = a_Uv;
                v_Color = a_Color;
                mat4 instance_transform = mat4(a_TCol1, a_TCol2, a_TCol3, a_TCol4);
                vec4 position = instance_transform * vec4(a_Pos, 0.0, 1.0);
                gl_Position = u_MVP * position;
            }
        ".to_vec();
        self
    }

    pub fn body(mut self, body: &'static [u8]) -> Self {
        self.body = body.to_vec();
        self
    }

    pub fn add_uniform(mut self, declare: &'static [u8]) -> Self {
        let mut declare = declare.to_vec();
        self.header.append(&mut declare);
        self
    }

    pub fn build(mut self) -> Result<Vec<u8>, String> {
        if self.body.is_empty() {
            return Err("body is empty".to_string());
        }
        self.whole_source.append(&mut self.header);
        self.whole_source.append(&mut self.body);
        Ok(self.whole_source)
    }
}

pub struct FragmentShaderBuilder {
    header: Vec<u8>,
    body: Vec<u8>,
    whole_source: Vec<u8>,
}

impl FragmentShaderBuilder {
    pub fn new_150() -> Self {
        Self {
            header: b"
                #version 150 core

                uniform sampler2D t_Texture;
                
                in vec2 v_Uv;
                in vec4 v_Color;
                out vec4 Target0;
            
                layout (std140) uniform Globals {
                    mat4 u_MVP;
                };
            ".to_vec(),
            body: b"".to_vec(),
            whole_source: b"".to_vec(),
        }
    }

    pub fn default_body(mut self) -> Self {
        self.body = b"
            void main() {
                vec4 base_color = texture(t_Texture, v_Uv);
                Target0 = base_color;
            }
        ".to_vec();
        self
    }

    pub fn body(mut self, body: &'static [u8]) -> Self {
        self.body = body.to_vec();
        self
    }

    pub fn add_uniform(mut self, declare: &'static [u8]) -> Self {
        let mut declare = declare.to_vec();
        self.header.append(&mut declare);
        self
    }

    pub fn build(mut self) -> Result<Vec<u8>, String> {
        if self.body.is_empty() {
            return Err("body is empty".to_string());
        }
        self.whole_source.append(&mut self.header);
        self.whole_source.append(&mut self.body);
        Ok(self.whole_source)
    }
}
