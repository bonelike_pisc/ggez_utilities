pub mod shader;

//use image;
//use ggez::Context;
//use ggez::error::GameResult;
//use ggez::graphics::Image;

/*
/// make ggez::graphics::Image object from not a file path but raw bytedata on memory
/// 
/// note:
///     An almost identical method Image::from_bytes() is supported in ggez v0.6. So this method is not necessary anymore.
pub fn make_image_from_memory(context: &mut Context, data: Vec<u8>) -> GameResult<Image> {
    let img = image::load_from_memory(&data).unwrap().to_rgba8();
    let (width, height) = img.dimensions();
    Image::from_rgba8(context, width as u16, height as u16, &img)
}
*/

/// generate vertexes and vertex index list for ggez::graphics::Mesh::from_raw()
/// 
/// Vertex positions are automatically determined, depending on the resol (resolution) value.
/// 
/// For example, if resol is 2, following 13 vertexes are generated:
/// 
/// (0.0, 0.0), (0.0, 0.5), (0.0, 1.0),
/// (0.25, 0.25), (0.25, 0.75),
/// (0.5, 0.0), (0.5, 0.5), (0.5, 1.0),
/// (0.75, 0.25), (0.75, 0.75),
/// (1.0, 0.0), (1.0, 0.5), (1.0, 1.0)
/// 
/// and for each in 0.5 x 0.5 cells, 4 triangles including the center point (e.g. (0.25, 0.25) in the left upper cell) are defined.
/// 
/// The generated vertexes are able to be used like:
/// 
/// ```
/// let (verts, vinds) = make_vertex_list(4).unwrap();
/// let mesh = ggez::graphics::Mesh::from_raw(ctx, &verts, &vinds, Some(texture)).unwrap();
/// ```
pub fn make_vertex_list(resol: u32) -> Result<(Vec<ggez::graphics::Vertex>, Vec<u32>), String> {
    if resol < 1 { return Err("resolution must be more than 0.".to_string()); }

    // vertexes
    let mut vertexes = Vec::new();
    let resol2 = 2 * resol;

    for ix in 0..(resol2 + 1) {
        for iy in 0..(resol2 + 1) {
            if (ix + iy) % 2 != 0 { continue; }
            let pos = [ix as f32 / resol2 as f32, iy as f32 / resol2 as f32];
            let vertex = ggez::graphics::Vertex { pos: pos, uv: pos, color: [1.0, 1.0, 1.0, 1.0] };
            vertexes.push(vertex);
        }
    }

    // vertex indexes
    let mut v_indexes = Vec::new();
    for ix in 0..resol {
        for iy in 0..resol {
            let i_top_left = ix * (2 *resol + 1) + iy;
            let i_bottom_left = i_top_left + 1;
            let i_center = i_top_left + resol + 1;
            let i_top_right = i_center + resol;
            let i_bottom_right = i_top_right + 1;
            // left triangle
            v_indexes.push(i_top_left);
            v_indexes.push(i_center);
            v_indexes.push(i_bottom_left);
            // top triangle
            v_indexes.push(i_top_left);
            v_indexes.push(i_top_right);
            v_indexes.push(i_center);
            // right triangle
            v_indexes.push(i_top_right);
            v_indexes.push(i_bottom_right);
            v_indexes.push(i_center);
            // bottom triangle
            v_indexes.push(i_bottom_left);
            v_indexes.push(i_center);
            v_indexes.push(i_bottom_right);
        }
    }

    Ok((vertexes, v_indexes))
}
