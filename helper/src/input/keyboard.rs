use ggez::input::keyboard;
use super::binary_switch::*;

pub struct KeyboardStatus {
    key: keyboard::KeyCode,
    status: bool,
}

impl KeyboardStatus {
    pub fn new(key: keyboard::KeyCode) -> Self {
        Self {
            key,
            status: false
        }
    }

    pub fn set(&mut self, context: &mut ggez::Context) {
        self.status = keyboard::is_key_pressed(context, self.key)
    }
}

impl BinaryStatus for KeyboardStatus {
    fn get(&self) -> bool {
        self.status
    }
}

pub struct KeyboardLog {
    log: BinarySwitchLog<KeyboardStatus>,
    history_capacity: usize, //保持される履歴数
}

impl KeyboardLog {
    pub fn new(key: keyboard::KeyCode) -> Self {
        Self {
            log: BinarySwitchLog::new(KeyboardStatus::new(key)),
            history_capacity: 100
        }
    }

    pub fn update(&mut self, context: &mut ggez::Context) {
        self.log.inner_mut().set(context);
    }

    pub fn restore(&mut self) {
        self.log.restore();
        if self.log.get_history().len() > self.history_capacity {
            self.log.purge(1);
        }
        //println!("keyboard log size: {}", self.log.get_history().len());
    }

    pub fn is_now_pressed(&self) -> bool {
        self.log.inner().get()
    }

    pub fn is_just_pressed(&self) -> bool {
        let now = self.log.inner().get();
        if ! now { return false; }
        let history = self.log.get_history();
        match history.back() {
            Some((last_status, _last_time)) => ! last_status,
            None => true
        }
    }

    pub fn is_just_released(&self) -> bool {
        let now = self.log.inner().get();
        if now { return false; }
        let history = self.log.get_history();
        match history.back() {
            Some((last_status, _last_time)) => *last_status,
            None => false
        }
    }

    pub fn elapsed(&self) -> Option<std::time::Duration> {
        let history = self.log.get_history();
        history.back().map(|(_last_status, last_time)| last_time.elapsed())
    }
}
