use std::collections::VecDeque;
use std::time::Instant;

// 2状態のいずれかをとるトレイト
pub trait BinaryStatus {
    fn get(&self) -> bool;
}

pub struct BinarySwitchLog<S: BinaryStatus> {
    status: S,
    history: VecDeque<(bool, Instant)>
}

impl <S> BinarySwitchLog<S> where S: BinaryStatus {
    pub fn new(status: S) -> Self {
        Self {
            status,
            history: VecDeque::new()
        }
    }

    pub fn inner(&self) -> &S {
        &self.status
    }

    pub fn inner_mut(&mut self) -> &mut S {
        &mut self.status
    }

    pub fn restore(&mut self) {
        let status = self.status.get();
        let time = Instant::now();
        match self.history.back() {
            Some((last_status, _last_time)) => {
                if status != *last_status {
                    self.history.push_back((status, time));
                }
            },
            None => {
                self.history.push_back((status, time));
            }
        }
    }

    pub fn purge(&mut self, n: usize) {
        for _i in 0..n {
            let item = self.history.pop_front();
            if item.is_none() {
                return;
            }
        }
    }

    pub fn get_history(&self) -> &VecDeque<(bool, Instant)> {
        &self.history
    }
}
