use gfx::*;
use ggez_helper::ggez::graphics;
use ggez_helper::ggez::{Context, GameResult};
use super::GgezSimpleDraw;

gfx_defines! {
    constant Param {
        width: f32 = "width", //輪郭と判定する領域の幅
    }
}

// 縁取り（輪郭を強調）で描画
pub struct Bordering<T> where T: GgezSimpleDraw {
    inner: T,
    param: Param,
    shader: graphics::Shader<Param>
}

impl <T> Bordering<T> where T: GgezSimpleDraw {
    pub fn new(context: &mut Context, inner: T, width: f32) -> GameResult<Self> {
        let param = Param { width: width };
        let vertex_shader = ggez_helper::graphics::shader::VertexShaderBuilder::new_150().default_body().build().unwrap();
        let fragment_shader = Self::fragment_shader();
        let shader = graphics::Shader::from_u8(
            context,
            &vertex_shader,
            &fragment_shader,
            param,
            "Param",
            None
        )?;
        Ok(Self {
            inner,
            param,
            shader
        })
    }

    pub fn inner(&mut self) -> &mut T {
        &mut self.inner
    }

    fn fragment_shader() -> Vec<u8> {
        ggez_helper::graphics::shader::FragmentShaderBuilder::new_150()
            .add_uniform(b"
                layout (std140) uniform Param {
                    float width;
                };
            ")
            .body(b"
                void main() {
                    vec4 base_color = texture(t_Texture, v_Uv);
                    vec4 neighbor_color1 = texture(t_Texture, vec2(v_Uv.x - width, v_Uv.y));
                    vec4 neighbor_color2 = texture(t_Texture, vec2(v_Uv.x + width, v_Uv.y));
                    vec4 neighbor_color3 = texture(t_Texture, vec2(v_Uv.x, v_Uv.y - width));
                    vec4 neighbor_color4 = texture(t_Texture, vec2(v_Uv.x, v_Uv.y + width));
                    vec4 neighbor_color5 = texture(t_Texture, vec2(v_Uv.x - width * 0.71, v_Uv.y - width * 0.71));
                    vec4 neighbor_color6 = texture(t_Texture, vec2(v_Uv.x - width * 0.71, v_Uv.y + width * 0.71));
                    vec4 neighbor_color7 = texture(t_Texture, vec2(v_Uv.x + width * 0.71, v_Uv.y - width * 0.71));
                    vec4 neighbor_color8 = texture(t_Texture, vec2(v_Uv.x + width * 0.71, v_Uv.y + width * 0.71));
                    float inner = neighbor_color1.w * neighbor_color2.w * neighbor_color3.w * neighbor_color4.w *
                        neighbor_color5.w * neighbor_color6.w * neighbor_color7.w * neighbor_color8.w;
                    Target0 = vec4((1.0 - inner) * vec4(0.0, 0.0, 0.0, base_color.w) + inner * base_color);
                }
            ")
            .build().unwrap()
    }
}

impl <T> GgezSimpleDraw for Bordering<T> where T: GgezSimpleDraw {
    fn draw(&self, context: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        let canvas = graphics::Canvas::with_window_size(context)?;
        graphics::set_canvas(context, Some(&canvas));
        graphics::clear(context, [0.0, 0.0, 0.0, 0.0].into());
        self.inner.draw(context, graphics::DrawParam::new(), Some(&canvas))?;

        graphics::set_canvas(context, target);
        {
            let _lock = graphics::use_shader(context, &self.shader);
            self.shader.send(context, self.param)?;
            graphics::draw(context, &canvas, param)?;
            
        }
        Ok(())
    }
}

