use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper;
use super::{GgezSimpleDraw, HasRectSize};

pub struct Image {
    inner: graphics::Image,
}

impl Image {
    pub fn new(inner: graphics::Image) -> Self {
        Self { inner }
    }
}

impl GgezSimpleDraw for Image {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        graphics::set_canvas(ctx, target);
        graphics::draw(ctx, &self.inner, param)
    }
}

impl HasRectSize for Image {
    fn get_rect_size(&self, _context: &Context) -> (f32, f32) {
        (self.inner.dimensions().w, self.inner.dimensions().h)
    }
}

pub struct Text {
    fragment: graphics::TextFragment
}

impl Text {
    pub fn new(text_str: &str, font: graphics::Font) -> Self {
        let fragment = graphics::TextFragment::new(text_str)
            .font(font)
            .scale(graphics::PxScale::from(16.0));
        Self {
            fragment
        }
    }

    pub fn color(self, color: [f32; 4]) -> Self {
        let new = Self {
            fragment: self.fragment.color(color.into())
        };
        new
    }

    pub fn px_scale(self, px_scale: f32) -> Self {
        let new = Self {
            fragment: self.fragment.scale(graphics::PxScale::from(px_scale))
        };
        new
    }

    pub fn set_text_str(&mut self, text_str: &str) {
        self.fragment.text = text_str.to_owned();
    }
}

impl GgezSimpleDraw for Text {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        let text = graphics::Text::new(
            self.fragment.clone()
        );

        graphics::set_canvas(ctx, target);
        graphics::draw(ctx, &text, param)
    }
}

impl HasRectSize for Text {
    fn get_rect_size(&self, context: &Context) -> (f32, f32) {
        let text = graphics::Text::new(
            self.fragment.clone()
        );
        (text.width(context), text.height(context))
    }
}
