use gfx::{self, *};
use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper as helper;
use super::GgezSimpleDraw;

gfx_defines! {
    constant MonotoneShaderParam {
        base_color: [f32; 3] = "u_color",
    }
}

pub struct Monotone<T> where T: GgezSimpleDraw {
    inner: T,
    param: MonotoneShaderParam,
    shader: graphics::Shader<MonotoneShaderParam>,
}

impl <T> Monotone<T> where T: GgezSimpleDraw {
    pub fn new(ctx: &mut Context, inner: T) -> GameResult<Self> {
        let param = MonotoneShaderParam { base_color: [0.5, 0.5, 0.5] };
        let shader = graphics::Shader::from_u8(
            ctx,
            &Self::vertex_shader(),
            &Self::fragment_shader(),
            param,
            "BaseColor",
            None
        )?;
        Ok(Self {
            inner,
            param,
            shader,
        })
    }

    pub fn get_base_color(&self) -> [f32; 3] {
        self.param.base_color
    }

    pub fn set_base_color(&mut self, base_color: [f32; 3]) {
        self.param.base_color = base_color;
    }

    pub fn inner(&mut self) -> &mut T {
        &mut self.inner
    }

    fn vertex_shader() -> Vec<u8> {
        helper::graphics::shader::VertexShaderBuilder::new_150().default_body().build().unwrap()
    }

    fn fragment_shader() -> Vec<u8> {
        helper::graphics::shader::FragmentShaderBuilder::new_150()
        .add_uniform(
            b"
                layout (std140) uniform BaseColor {
                    vec3 u_color;
                };
            "
        )
        .body(
            b"
                void main() {
                    vec4 tex_color = texture(t_Texture, v_Uv);
                    float mean_rgb = (tex_color.x + tex_color.y + tex_color.z) / 3.0;
                    float thre = (u_color.x + u_color.y + u_color.z) / 3.0;
                    vec3 draw_color1 = step(mean_rgb, thre) * u_color.xyz * mean_rgb / thre;
                    vec3 draw_color2 = step(thre, mean_rgb) * (u_color.xyz + (1.0 - u_color.xyz) * (mean_rgb - thre) / (1.0 - thre));
                    vec3 draw_color = vec3(draw_color1.xyz + draw_color2.xyz);

                    //Target0 = vec4(tex_color.xyzw);
                    Target0 = vec4(draw_color.xyz, tex_color.w);
                }
            "
        )
        .build().unwrap()
    }
}

impl <T> GgezSimpleDraw for Monotone<T> where T: GgezSimpleDraw {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        // いったんCanvasに描画
        let canvas = graphics::Canvas::with_window_size(ctx)?;
        graphics::set_canvas(ctx, Some(&canvas));
        graphics::clear(ctx, [0.0, 0.0, 0.0, 0.0].into());
        self.inner.draw(ctx, param, Some(&canvas))?;

        // CanvasにShaderを適用して描画
        //let (vertexes, v_indexes) = helper::graphics::make_vertex_list(1).unwrap();
        //let mesh = graphics::Mesh::from_raw(ctx, &vertexes, &v_indexes, Some(canvas.into_inner()))?;
        graphics::set_canvas(ctx, target);
        {    
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.param)?;
            graphics::draw(ctx, &canvas, graphics::DrawParam::default())?;
        }

        Ok(())
    }
}
