mod delay;
pub use delay::DelayAnimation;

use std::time::Instant;
use ggez_helper::ggez::{Context, GameResult};
use ggez_helper::ggez::graphics::Canvas;
use super::GgezNonParamDraw;

pub trait Animation {
    fn start(&mut self);
    fn stop(&mut self);
    fn is_stopped(&self) -> bool;
    fn update(&mut self);
    fn draw(&self, context: &mut Context, target: Option<&Canvas>) -> GameResult;
}

pub struct AnimationContext {
    pub delta_sec: f32,
    pub total_sec: f32,
    pub frames: u32,
}

pub struct CustomAnimation<T> where T: GgezNonParamDraw {
    inner: T,
    timer: Option<Instant>,
    total_time_sec: f32,
    total_frames: u32,
    updater: Box<dyn FnMut(&mut T, AnimationContext) -> bool>,
}

impl <T> CustomAnimation<T> where T: GgezNonParamDraw {
    pub fn new<F>(inner: T, updater: F) -> Self
        where F: FnMut(&mut T, AnimationContext) -> bool + 'static
    {
        Self {
            inner: inner,
            updater: Box::new(updater),
            total_time_sec: 0.0,
            total_frames: 0,
            timer: None,
        }
    }
}

impl <T> Animation for CustomAnimation<T> where T: GgezNonParamDraw {
    fn start(&mut self) {
        self.timer = Some(Instant::now());
    }

    fn stop(&mut self) {
        self.timer = None;
    }

    fn is_stopped(&self) -> bool {
        self.timer.is_none()
    }

    fn update(&mut self) {
        if let Some(t) = self.timer.as_ref() {
            let delta_time_sec = t.elapsed().as_secs_f32();
            self.timer = Some(Instant::now());
            self.total_time_sec += delta_time_sec;
            self.total_frames += 1;
            let context = AnimationContext {
                delta_sec: delta_time_sec,
                total_sec: self.total_time_sec,
                frames: self.total_frames
            };
            let continues = (self.updater)(&mut self.inner, context);
            if !continues {
                self.stop();
            }
        }
    }

    fn draw(&self, context: &mut Context, target: Option<&Canvas>) -> GameResult {
        if !self.is_stopped() {
            self.inner.draw(context, target)
        }
        else {
            Ok(())
        }
    }
}
