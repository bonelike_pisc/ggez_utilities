use std::time::Instant;
use ggez_helper::ggez::{Context, GameResult};
use ggez_helper::ggez::graphics::Canvas;
use super::Animation;

/*
 * 指定した時間後に始まるアニメーション
 */
pub struct DelayAnimation<T> where T: Animation {
    inner: T,
    delay_sec: f32,
    timer: Option<Instant>,
    started: bool,
    inner_started: bool
}

impl <T> DelayAnimation<T> where T: Animation {
    pub fn new(inner: T, delay_sec: f32) -> Self
    where
        T: Animation + 'static,
    {
        Self {
            inner,
            delay_sec,
            timer: None,
            started: false,
            inner_started: false
        }
    }
}

impl <T> Animation for DelayAnimation<T> where T: Animation {
    fn start(&mut self) {
        if !self.started {
            self.timer = Some(Instant::now());
            self.started = true;
        }
    }

    fn stop(&mut self) {
        if self.started {
            self.inner.stop();
            self.started = false;
            self.inner_started = false;
        }
    }

    fn is_stopped(&self) -> bool {
        !self.started || self.inner.is_stopped()
    }

    fn update(&mut self) {
        if !self.started {
            return;
        }
        if self.inner_started {
            if !self.inner.is_stopped(){
                self.inner.update();
            }
            return;
        }
        let elapsed = self.timer.unwrap().elapsed().as_secs_f32();
        if !self.inner_started && elapsed >= self.delay_sec {
            self.inner.start();
            self.inner_started = true;
        }
    }

    fn draw(&self, context: &mut Context, target: Option<&Canvas>) -> GameResult {
        if self.inner_started && !self.inner.is_stopped() {
            self.inner.draw(context, target)
        }
        else {
            Ok(())
        }
    }
}
