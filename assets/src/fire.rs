use gfx::{self, *};
use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper as helper;
use super::GgezSimpleDraw;

gfx_defines! {
    constant FireParam {
        intensity: f32 = "u_intensity",
    }
}

pub struct Fire {
    param: FireParam,
    area: graphics::Mesh,
    shader: graphics::Shader<FireParam>,
}

impl Fire {
    pub fn new(ctx: &mut Context) -> GameResult<Self> {
        let (vertexes, v_indexes) = helper::graphics::make_vertex_list(1).unwrap();
        let area = graphics::Mesh::from_raw(ctx, &vertexes, &v_indexes, None)?;
        let param = FireParam { intensity: 0.0 };
        let shader = graphics::Shader::from_u8(
            ctx,
            vertex_shader(),
            fragment_shader(),
            param,
            "FireParam",
            None,
        )?;
        Ok(Self {
            param,
            area,
            shader,
        })
    }

    pub fn set_intensity(&mut self, intensity: f32) {
        self.param.intensity = intensity;
    }
}

impl GgezSimpleDraw for Fire {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        let canvas = graphics::Canvas::with_window_size(ctx)?;
        graphics::set_canvas(ctx, Some(&canvas));
        graphics::clear(ctx, [0.0, 0.0, 0.0, 0.0].into());
        {    
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.param)?;
            graphics::draw(ctx, &self.area, param)?;
        }
        graphics::set_canvas(ctx, target);
        graphics::draw(ctx, &canvas, graphics::DrawParam::default())?;
        Ok(())
    }
}

fn vertex_shader() -> &'static [u8] {
    b"
#version 150 core

in vec2 a_Pos;
in vec2 a_Uv;

in vec4 a_Src;
in vec4 a_TCol1;
in vec4 a_TCol2;
in vec4 a_TCol3;
in vec4 a_TCol4;
in vec4 a_Color;

layout (std140) uniform Globals {
    mat4 u_MVP;
};

out vec2 v_Uv;
out vec4 v_Color;

void main() {
    //v_Uv = a_Uv * a_Src.zw + a_Src.xy;
    v_Uv = a_Uv;
    v_Color = a_Color;
    mat4 instance_transform = mat4(a_TCol1, a_TCol2, a_TCol3, a_TCol4);
    vec4 position = instance_transform * vec4(a_Pos, 0.0, 1.0);
    //vec4 position = vec4(a_Pos, 0.0, 1.0);

    gl_Position = u_MVP * position;
}
    "
}

fn fragment_shader() -> &'static [u8] {
    b"
#version 150 core
uniform sampler2D t_Texture;
uniform vec2 resolution;
in vec2 v_Uv;
in vec4 v_Color;
out vec4 Target0;

layout (std140) uniform Globals {
    mat4 u_MVP;
};

layout (std140) uniform FireParam {
    float u_intensity;
};

void main() {
    vec2 nuv = (v_Uv / 1.0);
    float a1 = -260.0/11.0;
    float b1 = -5.0/6.0 * a1 - 4.0/3.0;
    float c1 = 1.0/6.0 * a1 + 8.0/3.0;
    float a2 = 8.0 * a1;
    float b2 = -6.0 * a1 + 4.0 * b1;
    float c2 = 3.0/2.0 * a1 - 2.0 * b1 + 2.0 * c1;
    float d2 = -1.0/8.0 * a1 + 1.0/4.0 * b1 - 1.0/2.0 * c1;

    float profile1 = -25.0 * nuv.x * nuv.x + 25.0 * nuv.x -25.0 / 4.0
        + a1 * nuv.y * nuv.y * nuv.y + b1 * nuv.y * nuv.y + c1 * nuv.y;
    float cutoff1 = step(0.0, profile1) * profile1 * (0.95 + u_intensity * 0.05);
    float profile2 = -100.0 * nuv.x * nuv.x + 100.0 * nuv.x - 25.0
    //    + a2 * nuv.y * nuv.y * nuv.y + b2 * nuv.y * nuv.y + c2 * nuv.y + d2;
        - 100.0 * nuv.y * nuv.y + 100.0 * nuv.y - 25.0 + 1.0;
    float cutoff2 = step(0.0, profile2) * profile2 * (0.95 + u_intensity * 0.05);
    
    Target0 = vec4(1.0, cutoff1, 0.5 * cutoff2, cutoff1);
}
    "
}
