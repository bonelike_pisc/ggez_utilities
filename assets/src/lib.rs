mod fire;
mod indicator;
mod sphere;
mod trans_color;
mod fade;
mod primitive;
mod bordering;
pub mod animation;
pub use fire::Fire;
pub use indicator::PieIndicator;
pub use sphere::LightSphere;
pub use trans_color::Monotone;
pub use fade::{FadeInOutManager, Transparent};
pub use primitive::{Image, Text};
pub use bordering::Bordering;

use ggez_helper::ggez::{graphics, Context, GameResult};

pub trait GgezSimpleDraw {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult;
}

pub trait GgezNonParamDraw {
    fn draw(&self, ctx: &mut Context, target: Option<&graphics::Canvas>) -> GameResult;
}

pub trait HasRectSize {
    fn get_rect_size(&self, context: &Context) -> (f32, f32); //(width, height)
}

pub struct GgezNonParamDrawAdaptor<T> where T: GgezSimpleDraw {
    inner: T,
    pub dest: [f32; 2],
    pub scale: [f32; 2],
    pub rotation: f32,
    pub offset: [f32; 2]
}

impl <T> GgezNonParamDrawAdaptor<T> where T: GgezSimpleDraw {
    pub fn new(inner: T) -> Self {
        Self {
            inner,
            dest: [0.0, 0.0],
            scale: [1.0, 1.0],
            rotation: 0.0,
            offset: [0.0, 0.0]
        }
    }

    pub fn inner(&mut self) -> &mut T {
        &mut self.inner
    }
}

impl <T> GgezNonParamDraw for GgezNonParamDrawAdaptor<T> where T: GgezSimpleDraw {
    fn draw(&self, context: &mut Context, target: Option<&graphics::Canvas>) -> GameResult {
        let param = graphics::DrawParam::new()
            .dest(self.dest)
            .scale(self.scale)
            .rotation(self.rotation)
            .offset(self.offset);
        self.inner.draw(context, param, target)
    }
}