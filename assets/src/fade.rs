
// フェードイン・フェードアウトを適用する場合のalpha乗算値を管理する
pub struct FadeInOutManager {
    opacity: f32,
    delta_opacity: f32
}

impl FadeInOutManager {
    pub fn new(initial_opacity: f32) -> Self {
        let mut _initial_opacity = initial_opacity;
        if _initial_opacity > 1.0 {
            _initial_opacity = 1.0;
        }
        if _initial_opacity < 0.0 {
            _initial_opacity = 0.0;
        }
        Self {
            opacity: _initial_opacity,
            delta_opacity: 0.0
        }
    }

    pub fn get_opacity(&self) -> f32 {
        self.opacity
    }

    pub fn fade_in(&mut self, delta: f32) {
        self.delta_opacity = delta
    }

    pub fn fade_out(&mut self, delta: f32) {
        self.delta_opacity = - delta;
    }

    pub fn update(&mut self) {
        self.opacity += self.delta_opacity;
        if self.opacity > 1.0 {
            self.opacity = 1.0;
            self.delta_opacity = 0.0;
        }
        if self.opacity < 0.0 {
            self.opacity = 0.0;
            self.delta_opacity = 0.0;
        }
    }
}

use gfx::{self, *};
use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper as helper;
use super::GgezSimpleDraw;

gfx_defines! {
    constant TransparentShaderParam {
        opacity: f32 = "u_opacity",
    }
}

pub struct Transparent<T> where T: GgezSimpleDraw {
    inner: T,
    param: TransparentShaderParam,
    shader: graphics::Shader<TransparentShaderParam>,
}

impl <T> Transparent<T> where T: GgezSimpleDraw {
    pub fn new(ctx: &mut Context, inner: T) -> GameResult<Self> {
        let param = TransparentShaderParam { opacity: 1.0 };
        let shader = graphics::Shader::from_u8(
            ctx,
            &Self::vertex_shader(),
            &Self::fragment_shader(),
            param,
            "Opacity",
            None
        )?;
        Ok(Self {
            inner,
            param,
            shader,
        })
    }

    pub fn set_opacity(&mut self, opacity: f32) {
        let mut _opacity = opacity;
        if _opacity > 1.0 {
            _opacity = 1.0;
        }
        if _opacity < 0.0 {
            _opacity = 0.0;
        }
        self.param.opacity = _opacity;
    }

    pub fn inner(&mut self) -> &mut T {
        &mut self.inner
    }

    fn vertex_shader() -> Vec<u8> {
        helper::graphics::shader::VertexShaderBuilder::new_150().default_body().build().unwrap()
    }

    fn fragment_shader() -> Vec<u8> {
        helper::graphics::shader::FragmentShaderBuilder::new_150()
        .add_uniform(
            b"
                layout (std140) uniform Opacity {
                    vec3 u_opacity;
                };
            "
        )
        .body(
            b"
                void main() {
                    vec4 tex_color = texture(t_Texture, v_Uv);
                    Target0 = vec4(tex_color.xyz, tex_color.w * u_opacity);
                }
            "
        )
        .build().unwrap()
    }
}

impl <T> GgezSimpleDraw for Transparent<T> where T: GgezSimpleDraw {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        // いったんCanvasにinnerを描画
        let canvas = graphics::Canvas::with_window_size(ctx)?;
        graphics::set_canvas(ctx, Some(&canvas));
        graphics::clear(ctx, [0.0, 0.0, 0.0, 0.0].into());
        self.inner.draw(ctx, param, Some(&canvas))?;

        // CanvasにShaderを適用して描画
        graphics::set_canvas(ctx, target);
        {    
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.param)?;
            graphics::draw(ctx, &canvas, graphics::DrawParam::default())?;
        }

        Ok(())
    }
}