use gfx::{self, *};
use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper as helper;
use super::GgezSimpleDraw;

gfx_defines! {
    constant ShaderParam {
        base_color_r: f32 = "u_red",
        base_color_g: f32 = "u_green",
        base_color_b: f32 = "u_blue",
        intensity: f32 = "u_intensity",
    }
}

pub struct LightSphere {
    param: ShaderParam,
    area: graphics::Mesh,
    shader: graphics::Shader<ShaderParam>,
}

impl LightSphere {
    pub fn new(ctx: &mut Context, base_color: [f32; 3]) -> GameResult<Self> {
        let (vertexes, v_indexes) = helper::graphics::make_vertex_list(1).unwrap();
        let area = graphics::Mesh::from_raw(ctx, &vertexes, &v_indexes, None)?;
        let param = ShaderParam {
            base_color_r: base_color[0],
            base_color_g: base_color[1],
            base_color_b: base_color[2],
            intensity: 1.0
        };
        let shader = graphics::Shader::from_u8(
            ctx,
            &Self::vertex_shader(),
            &Self::fragment_shader(),
            param,
            "BaseColor",
            None,
        )?;
        Ok(Self {
            param,
            area,
            shader,
        })
    }

    pub fn get_intensity(&self) -> f32 {
        self.param.intensity
    }

    pub fn set_intensity(&mut self, intensity: f32) {
        self.param.intensity = intensity;
    }

    fn vertex_shader() -> Vec<u8> {
        helper::graphics::shader::VertexShaderBuilder::new_150().default_body().build().unwrap()
    }

    fn fragment_shader() -> Vec<u8> {
        helper::graphics::shader::FragmentShaderBuilder::new_150()
            .add_uniform(
                b"
                    layout (std140) uniform BaseColor {
                        float u_red;
                        float u_green;
                        float u_blue;
                        float u_intensity;
                    };
                "
            )
            .body(
                b"
                    void main() {
                        vec2 pos = vec2(0.5 - v_Uv.y, v_Uv.x - 0.5);
                        float r = length(pos);
                        float r2 = r * r * 4.0;
                        vec3 base_color = vec3(u_red, u_green, u_blue);
                        vec3 whitening = vec3((1.0 - base_color.xyz) * (1.0 - r * 2.0) * step(r, 0.5));
                        vec3 draw_color = base_color + u_intensity * step(0.0, u_intensity) * whitening;
                        float alpha = (1.0 - r2) * u_intensity * step(0.0, u_intensity) * 1.0;

                        Target0 = vec4(draw_color, alpha);
                    }
                "
            ).build().unwrap()
    }
}

impl GgezSimpleDraw for LightSphere {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        let canvas = graphics::Canvas::with_window_size(ctx)?;
        graphics::set_canvas(ctx, Some(&canvas));
        graphics::clear(ctx, [0.0, 0.0, 0.0, 0.0].into());
        {    
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.param)?;
            graphics::draw(ctx, &self.area, param)?;
        }
        graphics::set_canvas(ctx, target);
        graphics::draw(ctx, &canvas, graphics::DrawParam::default())?;
        Ok(())
    }
}
