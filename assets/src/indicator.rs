use gfx::{self, *};
use ggez_helper::ggez::{graphics, Context, GameResult};
use ggez_helper as helper;
use super::GgezSimpleDraw;

gfx_defines! {
    constant ShaderParam {
        value: f32 = "u_value",
    }
}

pub struct PieIndicator {
    param: ShaderParam,
    area: graphics::Mesh,
    shader: graphics::Shader<ShaderParam>,
}

impl PieIndicator {
    pub fn new(ctx: &mut Context) -> GameResult<Self> {
        let (vertexes, v_indexes) = helper::graphics::make_vertex_list(1).unwrap();
        let image = graphics::Image::new(ctx, "/pie_indicator.png")?; //テクスチャを貼る場合
        let area = graphics::Mesh::from_raw(ctx, &vertexes, &v_indexes, Some(image))?;
        let param = ShaderParam { value: 0.0 };
        let shader = graphics::Shader::from_u8(
            ctx,
            &Self::vertex_source(),
            &Self::fragment_source(),
            param,
            "Value",
            None
        )?;
        Ok(Self {
            param,
            area,
            shader,
        })
    }

    

    pub fn get_value(&self) -> f32 {
        self.param.value
    }

    pub fn set_value(&mut self, value: f32) {
        self.param.value = value;
    }

    fn vertex_source() -> Vec<u8> {
        helper::graphics::shader::VertexShaderBuilder::new_150().default_body().build().unwrap()
    }

    fn fragment_source() -> Vec<u8> {
        helper::graphics::shader::FragmentShaderBuilder::new_150()
            .add_uniform(
                b"
                    layout (std140) uniform Value {
                        float u_value;
                    };
                "
            )
            .body(
                b"
                    void main() {
                        vec4 tex_color = texture(t_Texture, v_Uv);
                        vec2 pos = vec2(0.5 - v_Uv.y, v_Uv.x - 0.5);
                        float r = length(pos);
                        // 0 <= theta < 2*pi
                        float theta = step(0.0, pos.y) * acos(pos.x / r) + step(pos.y, 0) * (radians(360) - acos(pos.x / r));
                        //float alpha = step(r, 0.5);
                        float alpha = tex_color.w;
                        vec3 back_color = vec3(0.00, 0.00, 0.05);
                        vec3 front_color = vec3(1.0 - u_value, u_value, 0.0);
                        vec3 merged_color = back_color * step(u_value, theta / radians(360)) + front_color * step(theta / radians(360), u_value);
                        float is_white = step(1.0 - tex_color.x, 0.2) * step(1.0 - tex_color.y, 0.2) * step(1.0 - tex_color.z, 0.2);
                        merged_color = is_white * merged_color + (1.0 - is_white) * tex_color.xyz;
                        Target0 = vec4(merged_color, alpha);
                    }
                "
            ).build().unwrap()
    }
}

impl GgezSimpleDraw for PieIndicator {
    fn draw(&self, ctx: &mut Context, param: graphics::DrawParam, target: Option<&graphics::Canvas>) -> GameResult {
        let canvas = graphics::Canvas::with_window_size(ctx)?;
        //canvas.set_filter(graphics::FilterMode::Nearest);
        graphics::set_canvas(ctx, Some(&canvas));
        graphics::clear(ctx, [0.0, 0.0, 0.0, 0.0].into());
        {    
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.param)?;
            graphics::draw(ctx, &self.area, param)?;
        }
        graphics::set_canvas(ctx, target);
        graphics::draw(ctx, &canvas, graphics::DrawParam::default())?;
        Ok(())
    }
}